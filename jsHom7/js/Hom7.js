"use strict";
console.dir(window);
function citiesList(array, dom = document.body) {
    let newArray = array.map(el => {
        let unit = el.split("-").toString();
        let item = document.createElement('p');
        item.innerHTML = `Famous cities of Spain - ${unit}`;
        item.className = "items";
        dom.append(item);
    });
    return newArray;
}
citiesList(["Madrid", "Barcelona", "Valencia", "Seville", "Bilbao", "Las Palmas"]);
