import { ADD_TO_CART, DELETE_FROM_CART, GET_DATA_TO_CART, GET_ITEMS, SET_IS_OPEN, TOGGLE_FAV } from "../actions/actions";

//cars items
export const getData = () => async (dispatch) => {
    const data = await fetch("cars.json").then(res => res.json())
    dispatch({type: GET_ITEMS, payload: data})
}
export const setCars = (arrayCars) =>(dispatch)=>{
    dispatch({type: GET_ITEMS, payload: arrayCars})
}
export const toggleFav = (Name)=> ({type:TOGGLE_FAV, payload:Name})


//modal 
export const setIsOpen = (isOpen)=>({type:SET_IS_OPEN, payload: isOpen,})


//cartItems 
export const addToCart = (Name)=>({type:ADD_TO_CART, payload:Name})

export const deleteFromCart = (Name)=>(dispatch)=>(dispatch({type:DELETE_FROM_CART, payload:Name}))

export const getDataToCart = (array)=>({type:GET_DATA_TO_CART, payload:array})