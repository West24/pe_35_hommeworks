//item cars
export const GET_ITEMS = 'GET_ITEMS';
export const SET_CARS = 'SET_CARS';
export const TOGGLE_FAV = 'TOGGLE_FAV';

//modal
export const SET_IS_OPEN = 'SET_IS_OPEN';


//cart
export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_FROM_CART = 'DELET_FROM_CART';
export const GET_DATA_TO_CART = "GET_DATA_TO_CART";
