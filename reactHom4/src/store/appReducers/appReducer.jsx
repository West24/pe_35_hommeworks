import { combineReducers } from "redux"
import { carsReducer } from "./carsReducer"
import { cartReducer } from "./cartReducer"
import { modalReducer } from "./modalReducer"

export const appReducer = combineReducers({
    items: carsReducer,
    modal: modalReducer,
    cart: cartReducer,
})
