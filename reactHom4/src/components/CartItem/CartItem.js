import React from 'react';
import styles from './CartItem.module.scss';
import PropTypes from 'prop-types'

const CartItem = (props) => {
    const { Name, count, openModal } = props;
    
    return (
        <div className={styles.root}>
            <div>
                <span className={styles.textColor}>{Name}</span>
            </div>
            <span className={styles.textColorCount}>{count}</span>
            <span className={styles.close} onClick={()=>{openModal(Name, 'close')}}>X</span>
        </div>
    )
}
CartItem.propTypes = {
    Name: PropTypes.string,
    count: PropTypes.number.isRequired,

}
export default CartItem;