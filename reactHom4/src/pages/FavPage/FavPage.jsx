
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";
import background from '../../assets/imgs/Background02.jpg'
function FavPage ({cars, openModal, isModalOpened, closeModal}){
    return(
        <div  style={{backgroundImage:background, height:"100vh"}} >
          <ItemsContainer cars={cars}  typeCard='favPage' openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} />
        </div>
    )
}
export default FavPage