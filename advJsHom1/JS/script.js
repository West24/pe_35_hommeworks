class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;

    }
    set name(name){
        this._name = name;
    }
    get name(){
        return this._name;
    }
    set salary(salary){
        this._salary = salary;
    }
    get salary(){
        return this._salary
    }
    set age(age){
        this._age = age;
    }
    get age(){
        return this._age;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name,age,salary);
        this.lang = lang.join(', ');
    }
    set salary(salary){
        this._salary = salary * 3;
    }
    get salary(){
        return _salary;
    }
}


const mike = new Programmer('Mike', 20, 2000, ['eng', 'rus']);

const rob = new Programmer('Rob', 24, 1500, ['eng', 'rus']);

const alex = new Programmer('Alex', 32, 3500, ['rus', 'eng']);

console.log(mike);
console.log(rob);
console.log(alex);
