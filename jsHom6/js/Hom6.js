"use strict";
console.dir(window);
function filterBy(array, type) {
    let newArray = array.filter((el => typeof el !== type));
    return newArray;
}
console.log(filterBy(["hello", "world", 23, "23", null, undefined, true, {
}], "string"));
console.log(filterBy(["hello", "world", 23, "23", null, undefined, true, {
}], "number"));
console.log(filterBy(["hello", "world", 23, "23", null, undefined, true, {
}], "object"));
console.log(filterBy(["hello", "world", 23, "23", null, undefined, true, {
}], "boolean"));
console.log(filterBy(["hello", "world", 23, "23", null, undefined, true, {
}], "undefined"));
