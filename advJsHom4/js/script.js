class Films{
    constructor(url){
        this.url = url;
    }
    async getData(root=document.getElementById('root')){
        let aw = await axios
            .get(this.url, {})
            .then(({data})=>{
                data.forEach(({name, characters, episodeId, openingCrawl}) => {
                    let ul = document.createElement('ul');
                    ul.textContent = `The film is: ${name}, Episode: ${episodeId}, Content: ${openingCrawl}`;
                    characters.forEach((element) => {
                        axios
                            .get(element, {})
                            .then(({data})=>{
                                let li = document.createElement('li');
                                li.textContent = data.name;
                                ul.append(li)
                            })

                    });
                    root.append(ul)


                });
            });
        return aw;

    }
}

const test = new Films('https://ajax.test-danit.com/api/swapi/films');

const promice = Promise.resolve(test.getData());
promice.then(()=>{
    console.log('ok');
    console.log(promice)
});

console.log(promice);



