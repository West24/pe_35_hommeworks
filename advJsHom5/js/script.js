const url_users = 'https://ajax.test-danit.com/api/json/users';

const url_posts = 'https://ajax.test-danit.com/api/json/posts';

const root = document.querySelector('#root');

class Card{
    constructor(url){
        this.url = url;
    }

    request(url){
        return axios.get(url)
    }

    createCart(title, body, author, postId){
        let div = document.createElement('div');
        let titleArt = document.createElement('h3');
        titleArt.textContent=title;

        let titleBody = document.createElement('p');
        titleBody.textContent= body;

        let authorData = document.createElement('p');
        authorData.textContent = `${author.name}, ${author.email}`;

        let btn = document.createElement('button');
        btn.textContent = 'Delete';
        div.append(authorData, titleArt, titleBody, btn);

        btn.addEventListener('click',()=>{
            axios.delete(url_posts + '/'+postId)
                .then(data =>{
                    div.remove()
                })
        });

        div.classList.add('card');
        root.append(div)


    }

    async render(){
        const posts = await this.request(url_posts);

        posts.data.forEach(async({title, body, userId, id}) => {

            const author = await this.request(url_users+'/'+ userId);
            console.log(author.data);
            this.createCart(title, body, author.data, id)
        });

    }
}

let test = new Card(url_posts);
test.render();
