import React from 'react';
import styles from './CartItem.module.scss';
import PropTypes from 'prop-types'

class CartItem extends React.Component {
    render () {
        const { Name, count } = this.props;

        return (
            <div className={styles.root}>
                <div>
                    <span className={styles.textColor}>{Name}</span>
                </div>
                <span className={styles.textColor}>{count}</span>
            </div>
        )
    }
    
}
CartItem.propTypes = {
    Name: PropTypes.string,
    count: PropTypes.number.isRequired,

}
export default CartItem;