import React from 'react';
import styles from './Item.module.scss';
import PropTypes from 'prop-types';
import { ReactComponent as StarAdd } from "../../assets/svg/star-plus.svg"

class Item extends React.Component { 

    render () {
        const { Name, Price, url,  openModal, isFavorite, toggleFav } = this.props;
        

        return (
            <div className={styles.root}>
                <div className={styles.favourites}>
                   {isFavorite ? <StarAdd className={styles.star_fav}  onClick={()=>{toggleFav(Name)}} /> :<StarAdd className={styles.star_notfav}  onClick={()=>{toggleFav(Name)}} />}
                </div>
                <p className={styles.textColor} >{ Name.toUpperCase()}</p>
                <img src={url} alt={Name} />
                <span className={styles.textColor}>{Price}$</span>
                <button onClick={()=>{openModal(Name)}}>Add to cart</button>
                
            </div>
        )
    }
    
}

Item.propTypes = {
    Name: PropTypes.string.isRequired,
    Price: PropTypes.number.isRequired,
    url : PropTypes.string.isRequired,
    openModal: PropTypes.func.isRequired,
    isFavorite: PropTypes.bool,
    toggleFav : PropTypes.func.isRequired,

}


export default Item;