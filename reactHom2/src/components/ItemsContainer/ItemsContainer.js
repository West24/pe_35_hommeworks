import React from 'react';
import styles from './ItemsContainer.module.scss';
import Item from "../Item";
import PropTypes from 'prop-types'


class ItemsContainer extends React.Component {
    
    render() {
        const {items, addItem, openModal, toggleFav} = this.props;
    
        return (
            <section className={styles.root}>
                <h1 className={styles.titleText}>CARS</h1>
                <div className={styles.container}>
                    {items && items.map(item => <Item key={item.id} toggleFav={toggleFav} {...item} addItem={addItem} openModal={openModal}  />)}
                </div>
            </section>
        )
    }
    
}

ItemsContainer.propTypes = {
    items: PropTypes.array,
    addItem : PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    toggleFav : PropTypes.func.isRequired,

}


export default ItemsContainer;