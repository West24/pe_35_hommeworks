import React from 'react';
import './App.scss';
import ItemsContainer from './components/ItemsContainer/ItemsContainer';
import Cart from './components/Cart/Cart'
import Modal from './components/modal/Modal';
import background from './assets/imgs/Background02.jpg'

class App extends React.Component {


  state = {
    cars: [],
    cartItems: [],
    isModalOpened: false,
    closeBtn: true,
    currentItem: '',
  }

  async componentDidMount() {
    let arrayCart = JSON.parse(localStorage.getItem('cartItems'))
    console.log(arrayCart)

    let arrayCars = JSON.parse(localStorage.getItem('cars'))
    console.log(arrayCars)
    if(!arrayCars) {
      const response = await fetch('cars.json')
      .then((e) => e.json())
      this.setState({
      cars: response,
    })

    }else{
      this.setState((state) => {
        return {
          ...state,
          cars:[...arrayCars]
        }
      });
    }
    
    if(arrayCart){
      const cartItemss = [...arrayCart];
      this.setState((state) => {
        return {
          ...state,
          cartItems: cartItemss
        }
      });
      
    }
      
    
  }


  componentDidUpdate(){
    console.log('2')
      localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems))
      localStorage.setItem('cars', JSON.stringify(this.state.cars))
      
  }

  addItem = (Name) => {
    const index = this.state.cartItems.findIndex(({ Name: arrayName }) => {
      return Name === arrayName;
    })
    if (index === -1) {
      this.setState((current) => (
        {
          ...current,
          cartItems: [
            ...current.cartItems,
            {
              Name,
              count: 1,
            }]
        }
        ))
        
    } else {

      const cartItems = [...this.state.cartItems];
      cartItems[index].count++

      this.setState((state) => {
        return {
          ...state,
          cartItems: cartItems
        }
       
      });
      
    }
    localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems))
    
  }

  openModal = (name) => {
    this.setState((current) => ({
      ...current,
      isModalOpened: true,
      currentItem: name,
    }))
  }

  closeModal = () => {
    this.setState((current) => ({
      ...current,
      isModalOpened: false,
    }))

  }

toggleFav = (Name)=> {
  const index = this.state.cars.findIndex(({ Name: arrayName }) => {
    return Name === arrayName;
  })
  console.log(index)
    const cars = [...this.state.cars];
   if(cars[index].isFavorite === false){
    cars[index].isFavorite = true
    this.setState((current) =>{
      return {
        ...current,
        cars:cars,
      }
    })
   }else{
    cars[index].isFavorite = false
    this.setState((current) =>{
      return {
        ...current,
        cars:cars,
      }
    })
   }
    
  
}
  

  render() {
    
    
    const { cars, cartItems, isModalOpened, closeBtn, currentItem } = this.state

    return (
      <div className="root" style={{ backgroundImage: `url(${background})` }}>
        {isModalOpened ? <Modal addItem={this.addItem} currentItem={currentItem} header={'Attention'} mainText={'Do you want to add this item to your cart? '} items={cars} shouldClose={closeBtn ? <span className={"close"} onClick={this.closeModal}>&times;</span> : ''} func={this.closeModal} /> : ''}
        <div>
          <ItemsContainer items={cars} toggleFav={this.toggleFav} addItem={this.addItem} openModal={this.openModal} isModalOpened={isModalOpened} closeModal={this.closeModal} />
        </div>
        <div>
          <Cart items={cartItems} />
        </div>
      </div>
    );
  }


}

export default App;
