"use strict";
console.dir(window);
const images = document.querySelectorAll(".image-to-show");
const bttnStart = document.querySelector(".bttn-start");
const bttnStop = document.querySelector(".bttn-stop");

let m = 1;
let time = 0;

function changeImg() {
    images.forEach(el => el.classList.remove("active"));
    images[m].classList.add("active");
    m++;
    if (m === images.length) {
        m = 0;
    }
}

function start() {
    if (time === 0) {
        time = setInterval(changeImg, 3000);
    }
}


function stop() {
    clearInterval(time);
    time = 0;
}

window.onload = function () {
    start()
};
bttnStart.onclick = function () {
    start()
};
bttnStop.onclick = function () {
    stop()
};

