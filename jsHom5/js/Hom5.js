"use strict";
console.dir(window);

function createNewUser() {
    const firstName = prompt("Enter your name");
    const lastName = prompt("Enter your surname");
    let birthday = prompt("When's your birthday?", "dd.mm.yyyy").split('.');
    let birthDate = new Date(birthday[2], birthday[1] - 1, birthday[0]);
    let age = new Date();

    let newUser = {
        birthDate: birthDate,
        firstName: firstName,
        lastName: lastName,

        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            let ageUser = age.getFullYear() - this.birthDate.getFullYear();
            if (age.getMonth() < birthDate.getMonth() ||
                age.getMonth() == birthDate.getMonth() && age.getDate() < birthDate.getDate()) {
                ageUser--;
            }
            return ageUser;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDate.getFullYear();
        }
    };
    return newUser;
}
const user = createNewUser();
console.log("LogIn:" + " " + user.getLogin());
console.log("Password:" + " " + user.getPassword());
console.log("Age:" + " " + user.getAge());