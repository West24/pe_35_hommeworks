import React from "react";
import s from './CartsPage.module.scss';
import CartItems from "../../components/CartItems/CartItems";
import Form from "../../components/Form/Form";

function CartPage(props) {
  const {carts, openModal} = props
  return (
   <div className={s.container}>
     <Form carts={carts} />
     {carts && carts.map( element => <CartItems {...element} openModal={openModal}/>)}
   </div>
  );
}

export default CartPage