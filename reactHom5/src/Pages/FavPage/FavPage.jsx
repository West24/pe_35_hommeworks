import React from "react";
import FavoriteItems from "../../components/FavoriteItems/FavoriteItems";

function CartPage(props) {
  const {favorite} = props
  return (
   <>
     <FavoriteItems favorite={favorite} />
   </>
  );
}

export default CartPage