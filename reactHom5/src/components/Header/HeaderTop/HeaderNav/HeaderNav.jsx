import React from "react";
import s from "../HeaderTop.module.scss";
import {NavLink, Link} from "react-router-dom";

class HeaderNav extends React.Component {
  render() {
    return(
     <nav className={s.headerMain}>
       <ul className={s.headerMainItems}>
         <li><Link to="/home">Home</Link></li>
         <li><NavLink to=''>CD's</NavLink></li>
         <li><NavLink to=''>DVD's</NavLink></li>
         <li><NavLink to=''>News</NavLink></li>
         <li><NavLink to=''>Portfolio</NavLink></li>
         <li><NavLink to=''>Contact us</NavLink></li>
       </ul>
     </nav>
    )
  }
}

export default HeaderNav