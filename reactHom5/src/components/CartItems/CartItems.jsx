import React from "react";
import s from './CartItems.module.scss';
import iconClose from './icon/close.png'
import Form from "../Form/Form";
// import Forms from "../Form/Form";

function CartItems(props) {
  const {name, count, openModal} = props


  return (
   <section>
     <div className={s.container}>
       {/*<Forms />*/}
       <div className={s.wrapper}>
          <div className={s.wrapperItems}>
            <div className={s.item}>
            <div className={s.infoItem}>
              <span>{count}x</span>
              <h3>{name}</h3>
              <span>by Artist</span>
            </div>
            </div>
              <img className={s.deleteItem} style={{width: '12px', height: '12px'}} src={iconClose} alt="close icon" onClick={()=>{
                openModal(name, 'close')}}/>
          </div>
       </div>
     </div>
   </section>
  );
}

export default CartItems