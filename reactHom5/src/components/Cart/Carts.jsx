import React from "react";
import s from './Carts.module.scss'
import iconCart from './icon/cart.png'
import {NavLink} from "react-router-dom";

function Cart (props) {
  const { carts, cards} = props

  return(
     <section className={s.cartWrapper}>
       <div className={s.container}>
         <div className={s.cartItem}>
           <div>
             <img src={ iconCart } alt="icon"/>
           </div>
           <NavLink to="/cart">Cart <span>{ carts.length }</span></NavLink>
           <NavLink to="/favorite">Favorite <span>{cards.length}</span></NavLink>
         </div>
       </div>
     </section>

  )
}

export default Cart