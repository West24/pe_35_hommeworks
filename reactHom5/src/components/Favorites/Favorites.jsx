import React from "react";
import {ReactComponent as StarSolid} from "./icon/star-solid.svg";
import {ReactComponent as StarAdd} from "./icon/star-solid-add.svg";

function Favorites (props){
    const {name, url, value, id, isFavorite, removeItemFav, addFavorites} = props
  console.log(isFavorite)
    return(
     <div>
       {!isFavorite && <StarSolid style={{ width: '25px', cursor: 'pointer' }} onClick={()=>{addFavorites(name, url, value, id)}} />}
       {isFavorite && <StarAdd style={{ width: '25px', cursor: 'pointer' }} onClick={()=>{removeItemFav(name, id)}} />}
     </div>
    )
  }

export default Favorites