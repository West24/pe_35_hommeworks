import React from "react";
import s from "./Modal.module.scss"
import closeIcon from './icon/close.png'
import {useDispatch} from "react-redux";
import {addToCart, deleteFromCart} from "../../AppStore/actionsCreator/actionsCreator";

function Modal(props) {
  const { current, modalType, modal, closeModal } = props
  const dispatch = useDispatch()
  if (!modal) return null
  let method
  let textModal
  if(modalType === 'addItem'){
    method = addToCart
    textModal = 'Add'
  } else if(modalType === 'deleteItem') {
    method = deleteFromCart
    textModal = 'Remove'
  }
  return (
   <section>
     <div className={s.root}>
       <div className={s.background}/>
       <div className={s.content}>
         <div className={s.closeWrapper}>
           <button onClick={closeModal}>
             <img width='18' src={closeIcon} alt="icon close"/>
           </button>
         </div>
         <h2>{textModal}<span className={s.nameAlbum}>{current}</span>?</h2>
         <div className={s.buttonContainer}>
           <button onClick={() => {
             dispatch(method(current))
             closeModal()
           }}>Yes
           </button>
           <button>No</button>
         </div>
       </div>
     </div>

   </section>
  )
}

export default Modal