import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import CartPage from "../Pages/CartPage/CartPage";
import MainPage from "../Pages/MainPage/MainPage";
import FavPage from "../Pages/FavPage/FavPage";


function Routes(props){
  const {carts, removeItem, cards,
    addCarts,
    openModal,
    addFavorites} = props
  return(
   <Switch>
     <Route exact path="/home">
       <MainPage  cards={cards} addCarts={addCarts} openModal={openModal} addFavorites={addFavorites}/>
     </Route>
     <Route exact path="/cart">
       <CartPage carts={carts} removeItem={removeItem} />
     </Route>
     <Route>
       <FavPage />
     </Route>
   </Switch>

  );
}

export default Routes