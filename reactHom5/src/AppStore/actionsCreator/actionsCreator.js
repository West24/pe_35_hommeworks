import {
  GET_ITEMS,
  SET_IS_OPEN,
  ADD_TO_CART,
  TOGGLE_FAV,
  DELETE_FROM_CART,
  GET_DATA_TO_CART,
  GET_DATA_TO_FAV, CLEAR_CART,
} from '../actions/actions'

export const getData = () => async (dispatch) => {
  const data = await fetch('library.json').then(res => res.json())
  dispatch({type: GET_ITEMS, payload: data})
}
export const setCards = arr => (dispatch) => dispatch({type: GET_ITEMS, payload: arr})

export const setOpen = isOpen => ({type: SET_IS_OPEN, payload: isOpen})

export const addToCart = (name) => ({type: ADD_TO_CART, payload: name})
export const addToFavorites = (name, url, value, id) => ({type: TOGGLE_FAV, payload: name, url, value, id})
export const getDataToFav = array => ({type: GET_DATA_TO_FAV, payload: array})
export const deleteFromCart = name => ({type: DELETE_FROM_CART, payload: name})
export const clearCart = name => ({type: CLEAR_CART, payload: name })
export const getDataToCart = array => ({type: GET_DATA_TO_CART, payload: array})
