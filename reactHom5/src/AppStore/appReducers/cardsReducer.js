import { GET_ITEMS, TOGGLE_FAV,  GET_DATA_TO_FAV } from "../actions/actions";

const initialState = {
  cards:[],
  favorite: []
};

export const cardsReducer = (state = initialState, action) => {
    if (action.type === GET_ITEMS){
      return {...state, cards: [...action.payload]}
    }
    else if (action.type === TOGGLE_FAV){
      const index = state.cards.findIndex(({ name }) => {
        return name === action.payload;
      })
      const newCards = [...state.cards];
      let id = action.id
      if(newCards[index].isFavorite === false){
        newCards[index].isFavorite = true
        state.favorite.push(action)
        return {...state, cards:[...newCards]}
      }else {
        newCards[index].isFavorite = false
        state.favorite.splice(index, id)
        return {...state, cards:[...newCards]}
      }
    }  else if(action.type === GET_DATA_TO_FAV){
      return {...state, favorite: [...action.payload]}
    } else {
    return state
    }

}
