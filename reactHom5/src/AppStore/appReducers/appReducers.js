import {combineReducers} from "redux"
import {cardsReducer} from "./cardsReducer"
import {cartsReducer} from "./cartsReducer"
import {modalReducer} from "./modalReducer"

export const appReducers = combineReducers({
  items: cardsReducer,
  cart: cartsReducer,
  modal: modalReducer,
})