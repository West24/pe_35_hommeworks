"use strict";
console.dir(window);
const input = document.querySelector(".price");
const inputField = document.querySelector(".inputField");
const span = document.createElement("span");
const spanField = document.querySelector(".spanField");
const exitBttn = document.createElement("div");

input.addEventListener("focus", () => {
    input.style.border = "2px solid green";
});
input.addEventListener("blur", () => {
    let inputValue = +input.value;
    console.log(typeof inputValue);
    if (inputValue > 0) {
        input.style.border = "2px solid white";
        span.innerHTML = `Current Price: ${inputValue}`;
        spanField.append(span);
        span.style.fontSize = "20px";
        span.style.color = "white";
        span.style.border = "2px solid grey";
        span.style.backgroundColor = "green";
        span.style.borderRadius = "5px";
        span.style.position = "relative";
        span.style.padding = "5px";
        input.style.color = "green";
        spanField.append(span);
        input.style.color = "green";
        exitBttn.classList.add("exitBttn");
        spanField.append(exitBttn);
    } else {
        input.style.border = "2px solid red";
        span.textContent = "Please enter correct price";
        span.textContent = "Please enter correct price!!!";
        span.style.color = "white";
        span.style.backgroundColor = "red";
        span.style.padding = "5px";
        span.style.fontSize = "15px";
        span.style.border = "2px solid grey";
        span.style.borderRadius = "5px";
        inputField.after(span);
    }
});
exitBttn.addEventListener("click", () => {
    span.remove();
    exitBttn.remove();
    input.value = '';
});
