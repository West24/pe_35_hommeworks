'use strict';

const dropDown = document.querySelector('#drop-down');
const dropDownList = document.querySelector('#drop-down-list');
dropDown.addEventListener('click', dropDownFn);
function dropDownFn() {
    dropDown.classList.toggle('drop-down-active');
    dropDownList.classList.toggle('header__nav__list-active')
}