'use strict';

const {src, dest, series, watch, parallel} = require('gulp'),
    sass = require('gulp-sass')(require('sass')),
    browserSync = require('browser-sync').create(),
    gulpJsMinify = require('gulp-js-minify'),
    cleancss = require('gulp-clean-css'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify-es').default,
    imagemin = require('gulp-image'),
    newer = require('gulp-newer'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer');



function clear() {
    return src('./dist')
        .pipe(clean())
}

const styles = () => {
    return src("./src/scss/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({
            overrideBrowserslist: ["last 10 versions"],
            grid: true
        }))
        .pipe(concat("styles.min.css"))
        .pipe(cleancss({
            level: {
                2: {
                    specialComments: 0
                }
            }
        }))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream())
};


function scripts() {
    return src('./src/js/**/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulpJsMinify())
        .pipe(dest('./dist/script'))
        .pipe(browserSync.reload({
            stream: true
        }))

}
function browsersync() {
    browserSync.init({
        server: {
            baseDir: "./",
        },
        notify: false
    });
}

const startWatch = () => {
    watch("./src/html/**/*.html");
    watch("./src/scss/**/*.scss", parallel(styles));
    watch("./src/js/**/*.js", parallel(scripts));
    watch("./src/img/**/*.{gif,jpg,png,svg}", parallel(images));
    watch("./*.html").on("change", browserSync.reload);
};


const images = () => {
    return src("./src/img/**/*")
        .pipe(newer("./dist/img"))
        .pipe(imagemin())
        .pipe(dest("./dist/img"))
};


exports.default = series(clear, scripts, styles,images);
exports.dev = parallel(browsersync, startWatch, series(clean, scripts, styles, images));


