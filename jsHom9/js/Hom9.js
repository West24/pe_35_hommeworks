"use strict";
console.dir(window);
const tabs = document.querySelectorAll(".tabs-title");
const text = document.querySelectorAll(".tabs-content li");


tabs.forEach(function (tab) {
    tab.addEventListener("click", () => {
        let selectTab = tab;
        let category = selectTab.getAttribute("data-tab");
        let selectCategory = document.querySelector(category);
        if (!selectTab.classList.contains("active")) {
            tabs.forEach(tab => tab.classList.remove("active"));
            text.forEach(text => text.classList.remove("active"));
            selectTab.classList.add("active");
            selectCategory.classList.add("active");
        }
    })
});
