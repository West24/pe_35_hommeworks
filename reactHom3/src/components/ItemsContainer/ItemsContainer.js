import React from 'react';
import styles from './ItemsContainer.module.scss';
import Item from "../Item";
import PropTypes from 'prop-types'


function ItemsContainer (props) {
    
   
        const {cars, addItem, openModal, toggleFav, typeCard} = props;
        console.log(props)
        if(typeCard === 'mainPage'){
            return (
                <section className={styles.root}>
                    <h1 className={styles.titleText}>CARS</h1>
                    <div className={styles.container}>
                        {cars && cars.map(cars =>  <Item key={cars.id} toggleFav={toggleFav} {...cars} addItem={addItem} openModal={openModal}  />)}
                    </div>
                </section>
            )
        }else if(typeCard === 'favPage'){
            console.log(typeCard)
            return (
                <section className={styles.root}>
                    <h1 className={styles.titleText}>CARS</h1>
                    <div className={styles.container}>
                        {cars && cars.map((cars) =>  {
                            if(cars.isFavorite === true){
                            return <Item key={cars.id} toggleFav={toggleFav} {...cars} addItem={addItem} openModal={openModal}  />
                        }

                         })}
                    </div>
                </section>
            )
        }            


        
    
    
}

ItemsContainer.propTypes = {
    items: PropTypes.array,
    addItem : PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    toggleFav : PropTypes.func.isRequired,

}


export default ItemsContainer;