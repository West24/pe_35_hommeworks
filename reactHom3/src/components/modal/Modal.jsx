import React from "react";
import PropTypes from 'prop-types'
import './Modal.scss'



class Modal extends React.Component { 

    

render (){
    const {header, shouldClose, func, addItem, deleteItem, currentItem, modalType} = this.props
    
    let method;
    let mainText;
    console.log(method)
    if(modalType === 'addItem'){
        method = addItem
        mainText = 'Are you sure you want to add this item to the cart?'
    }else{
        method = deleteItem
        mainText = 'Are you sure you want to delete this item from the cart?'
    }
    console.log(method)

    return(
        <div className = {"modal active"}  onClick={func}>
            <div className={"modalContent"}>
            <div className={"modalContentHeader"} >
            <header className={'modalHeader'}> {header} </header>
                {shouldClose}
                </div>
            
            
            <p className={'mainText'}>{mainText}</p>
            <div className={'modalButtons'}>
             
            <button onClick={() => {method(currentItem)}} style={{backgroundColor:'grey', width:'70px', height:'25px'}} >OK</button>
            <button style={{backgroundColor:'grey', width:'70px', height:'25px'}} >Cancel</button>
            </div>
           
            </div>
        </div>
    )
}
    
}


Modal.propTypes = {
    header: PropTypes.string,
    shouldClose: PropTypes.object,
    func: PropTypes.func.isRequired,
    mainText: PropTypes.string,
    addItem : PropTypes.func.isRequired,
    currentItem: PropTypes.string.isRequired, 
}

export default Modal;