import React from "react";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";

function MainPage({cars, toggleFav, addItem, openModal, isModalOpened, closeModal }){

    return(
        <div>
          <ItemsContainer typeCard='mainPage' cars={cars} toggleFav={toggleFav} addItem={addItem} openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} />
        </div>


    )

}
export default MainPage