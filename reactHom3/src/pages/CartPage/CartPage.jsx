import react from "react";
import styles from './CartPage.scss'
import background from '../../assets/imgs/Background02.jpg'
import Cart from "../../components/Cart/Cart";

function CartPage (props) {
    const { cartItems, openModal } = props

    return(
        <div className={styles.cartPage_main} style={{backgroundImage:background, height:"100vh"}}>
            <div>
                 <Cart openModal={openModal} cartItems={cartItems} />
            </div>
        </div>
    )
}
export default CartPage